The aim of this project has been to develop a software using python and ROS (Robot Operating System), that has the purpose of computing the inversal kinematics for the KUKA YouBot, and to allow it to perform a pick and place manipulation task, picking an object in a given position and placing it in another specified position on its base.
All the informations about the implementation and details on the used tools can be find in the ![final report](./Final_Report.pdf)

![youbot](./YouBot.jpg)

